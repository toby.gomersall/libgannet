pub mod errors;
pub mod uni_channel;
pub mod multi_channel;
pub mod operation;

mod descriptors;
mod device_core;
mod mem_setup;
mod raw_device;
mod utils;
mod scatter_gather;
mod mem_pool;

pub use scatter_gather::ScatterGatherError;
pub use descriptors::DescriptorError;
pub use mem_setup::MemSetupError;
pub use errors::*;
pub use mem_pool::{
    MemPool,
    MemBlock,
    MemBlockLayout,
};

pub use operation::Operation;
use mem_setup::{RegisterLayout, RegisterCheck};

#[derive(Debug, Copy, Clone)]
enum TransferType {
    MM2S,
    S2MM,
}


