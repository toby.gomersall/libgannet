mod descriptors;
mod operation;
mod registers;
mod running;

pub use descriptors::MAX_BYTES_TRANSFER_PER_DESCRIPTOR;
pub use operation::{
    Channel,
    MultiChannelOperation,
    StaleMultiChannelOperation,
    MultiChannelOperationError,
    MultiChannelOperationRefreshError,
};

pub use running::{RunningDevice, ErroredRunningDevice};

use std::{
    error::Error,
    fmt,
    fs::{File, OpenOptions},
    mem::size_of,
    path::PathBuf,
    time,
};

use registers::{
    CommonControlRegister,
    CommonStatusRegister,
    ErrorRegister,
    Mm2sChannelControl,
    Mm2sChannelStatus,
    Registers,
    S2mmChannelControl,
    S2mmChannelStatus,
};

use descriptors::{
    Mm2sDescriptorControlField,
    Mm2sDescriptorFields,
    Mm2sDescriptorStatusField,
    S2mmDescriptorFields,
    S2mmDescriptorStatusField,
};

use crate::descriptors::{Descriptor, DescriptorPool};
use crate::device_core::{
    DeviceSpecifics,
    DmaDevice,
};
use crate::errors::{DeviceError, DmaCoreError};
use crate::mem_setup::{
    setup_cfg_and_data_mem,
    setup_descriptors_mem
};
use crate::{
    MemPool,
    MemBlock,
    MemBlockLayout,
    TransferType,
};
use crate::raw_device::RawDevice;
use crate::scatter_gather::{
    write_regular_blocks_descriptor_chain,
    DescriptorFunctions,
    ScatterGatherDevice,
    ScatterGatherConfig,
    ScatterGatherDeviceConfig,
};
use crate::operation::{
    Operation,
    OperationWithMemory,
    ScatterGatherOperation,
    MemoryFromOperation,
};
use crate::utils::{
    enable_interrupts,
    get_and_consume_interrupts,
    is_multi_channel_device
};

#[cfg(feature="async")]
use tokio::io::unix::AsyncFd;

/// The maximum number of channels that the DMA engine can support. Note that
/// you will only be able to use the number of channels configured in the
/// FPGA, which will be up to `MAX_CHANNELS`.
pub const MAX_CHANNELS: u8 = registers::Registers::N_CHANNELS;

const U32_SIZE: usize = size_of::<u32>();

/// Error during a multichannel DMA transfer. This error type encapsulates
/// the device that is recovered
#[derive(Debug)]
pub struct MultiChannelTransferError {
    pub source: DeviceError,
    pub errored_device: ErroredRunningDevice,
}

impl Error for MultiChannelTransferError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        Some(&self.source)
    }
}

impl fmt::Display for MultiChannelTransferError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error during transfer: {}", self.source)
    }
}

// This function writes to and reads the cfg registers to check if multi
// channel for either MM2S or S2MM is available
fn check_mc_transfer_type(
    raw_device: &RawDevice<Registers>,
    transfer_type: TransferType,
) -> Result<bool, DeviceError> {
    // Determine which channel enable/disable register needs to be written to
    // depending on the transfer type.
    let ch_en_reg = match transfer_type {
        TransferType::MM2S => Registers::Mm2sChannelEnable,
        TransferType::S2MM => Registers::S2mmChannelEnable,
    };

    // In the multi-channel device:
    //     Offset 0x008 = MM2S ChannelEnable register
    //         bit 0 is Read/Write
    //     Offset 0x508 = S2MM ChannelEnable register
    //         bit 0 is Read/Write
    //
    // In the non multi-channel device:
    //     Offset 0x008 = Mm2sSGCurrentDescriptorLsw register
    //         bit 0 is Read only
    //     Offset 0x508 = Outside device register space but less than one page
    //                    size (4096). Therefore it is safe to write to this
    //                    address as we assign a full page to each device.
    //
    // Based on the above if we can set bit 0 in the register at offsets 0x008
    // or 0x508 then the device must be multi channel and transfer_type must
    // be enabled. If we cannot then the device is either not multi channel or
    // transfer_type is not enabled on the device.
    raw_device.write_reg(ch_en_reg.clone(), 1);

    // Read the channel enabled register
    match raw_device.read_reg(ch_en_reg) {
        1u32 => Ok(true),
        0u32 => Ok(false),
        _ => panic!("An unexpected value was read from the register."),
    }
}

// This function returns the transfer type of a multi channel device
fn get_transfer_type(raw_device: &RawDevice<Registers>) -> Result<TransferType, DeviceError> {
    // Check if MM2S and S2MM are enabled
    let mc_mm2s_enabled = check_mc_transfer_type(raw_device, TransferType::MM2S)?;
    let mc_s2mm_enabled = check_mc_transfer_type(raw_device, TransferType::S2MM)?;

    if mc_mm2s_enabled && !mc_s2mm_enabled {
        Ok(TransferType::MM2S)
    } else if !mc_mm2s_enabled && mc_s2mm_enabled {
        Ok(TransferType::S2MM)
    } else {
        // libgannet cannot handle the situation when we have both S2MM and
        // MM2S enabled or the situation when we have neither.
        return Err(DeviceError::UnsupportedCapability);
    }
}

/// This function reads the S2MM descriptor status and returns the correct
/// error if any have been reported. If there are no errors it returns the
/// number of bytes transferred by the descriptor
pub(crate) fn mm2s_descriptor_status(descriptor: &Descriptor) -> Result<usize, DmaCoreError> {
    let descriptor_data = descriptor.as_slice();

    // The offset is in bytes, not words, so we need to divide
    // by U32_SIZE.
    let descriptor_status = descriptor_data[Mm2sDescriptorFields::Status.offset() / U32_SIZE];

    // Check the error status of the descriptor and return the
    // relevant error.
    if Mm2sDescriptorStatusField::InternalError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Internal)
    } else if Mm2sDescriptorStatusField::SlaveError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Slave)
    } else if Mm2sDescriptorStatusField::DecodeError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Decode)
    } else if Mm2sDescriptorStatusField::Complete.bitmask() & descriptor_status !=
        Mm2sDescriptorStatusField::Complete.bitmask()  {
        Err(DmaCoreError::DescriptorNotCompleted)
    } else {
        // Return the nbytes transferred by this descriptor
        Ok((Mm2sDescriptorStatusField::TransferredBytes.bitmask() & descriptor_status) as usize)
    }
}

/// This function reads the S2MM descriptor status and returns the correct
/// error if any have been reported. If there are no errors it returns the
/// number of bytes transferred by the descriptor
pub(crate) fn s2mm_descriptor_status(descriptor: &Descriptor) -> Result<usize, DmaCoreError> {
    let descriptor_data = descriptor.as_slice();

    // The offset is in bytes, not words, so we need to divide
    // by U32_SIZE.
    let descriptor_status = descriptor_data[S2mmDescriptorFields::Status.offset() / U32_SIZE];

    // Check the error status of the descriptor and return the
    // relevant error.
    if S2mmDescriptorStatusField::InternalError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Internal)
    } else if S2mmDescriptorStatusField::SlaveError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Slave)
    } else if S2mmDescriptorStatusField::DecodeError.bitmask() & descriptor_status != 0 {
        Err(DmaCoreError::Decode)
    } else if S2mmDescriptorStatusField::Complete.bitmask() & descriptor_status !=
        S2mmDescriptorStatusField::Complete.bitmask() {
        Err(DmaCoreError::DescriptorNotCompleted)
    } else {
        Ok((S2mmDescriptorStatusField::TransferredBytes.bitmask() & descriptor_status) as usize)
    }
}

/// This function overwrites the complete bit in the MM2S descriptor status to
/// refresh it for the next run
pub(crate) fn refresh_mm2s_descriptor(descriptor: &mut Descriptor) {
    let descriptor_data = descriptor.as_mut_slice();

    // The offset is in bytes, not words, so we need to divide
    // by U32_SIZE.
    descriptor_data[Mm2sDescriptorFields::Status.offset() / U32_SIZE] = 0u32;
}

/// This function overwrites the complete bit in the S2MM descriptor status to
/// refresh it for the next run
pub(crate) fn refresh_s2mm_descriptor(descriptor: &mut Descriptor) {
    let descriptor_data = descriptor.as_mut_slice();

    // The offset is in bytes, not words, so we need to divide
    // by U32_SIZE.
    descriptor_data[S2mmDescriptorFields::Status.offset() / U32_SIZE] = 0u32;
}

fn start_of_frame_bitmask(transfer_type: TransferType) -> u32 {
    // Return the start of frame bitmask for MM2S transfers. SoF bit is
    // not set for S2MM transfers
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorControlField::StartOfFrame.bitmask(),
        TransferType::S2MM => 0_u32,
    }
}

fn end_of_frame_bitmask(transfer_type: TransferType) -> u32 {
    // Return the end of frame bitmask for MM2S transfers. EoF bit is
    // not set for S2MM transfers
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorControlField::EndOfFrame.bitmask(),
        TransferType::S2MM => 0_u32,
    }
}

fn desc_field_nxt_desc_lsw_offset(transfer_type: TransferType) -> usize {
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorFields::NextDescriptorLsw.offset(),
        TransferType::S2MM => S2mmDescriptorFields::NextDescriptorLsw.offset(),
    }
}

fn desc_field_nxt_desc_msw_offset(transfer_type: TransferType) -> usize {
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorFields::NextDescriptorMsw.offset(),
        TransferType::S2MM => S2mmDescriptorFields::NextDescriptorMsw.offset(),
    }
}

fn desc_field_buffer_addr_lsw_offset(transfer_type: TransferType) -> usize {
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorFields::BufferAddrLsw.offset(),
        TransferType::S2MM => S2mmDescriptorFields::BufferAddrLsw.offset(),
    }
}

fn desc_field_buffer_addr_msw_offset(transfer_type: TransferType) -> usize {
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorFields::BufferAddrMsw.offset(),
        TransferType::S2MM => S2mmDescriptorFields::BufferAddrMsw.offset(),
    }
}

fn desc_field_control_offset(transfer_type: TransferType) -> usize {
    match transfer_type {
        TransferType::MM2S => Mm2sDescriptorFields::Control.offset(),
        TransferType::S2MM => S2mmDescriptorFields::Control.offset(),
    }
}

/// The Device structure encapsulates all the information required for the
/// system to perform scatter gather DMA operations. There are methods on the
/// Device structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    raw_device: RawDevice<Registers>,
    dma_descriptor_pool: DescriptorPool,
    dma_mem_pool: MemPool,
    dma_data_phys_addr: usize,
    transfer_type: TransferType,
    file: File,
    #[cfg(feature="async")]
    async_file: Option<AsyncFd<File>>,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being reset anyway
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {
    // This function write a 1 to the device reset bit
    fn write_reset_bit(&self) {
        // Determine which control register needs to be written to depending
        // on the device capability.
        let common_control_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonControl,
            TransferType::S2MM => Registers::S2mmCommonControl,
        };

        // Reset the channel
        self.raw_device
            .write_reg(common_control_reg, CommonControlRegister::Reset.bitmask());
    }

    // This function checks the status register to see if the device is halted
    fn halted(&self) -> bool {
        // Determine which status register needs to be read from depending on
        // the device capability.
        let common_status_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonStatus,
            TransferType::S2MM => Registers::S2mmCommonStatus,
        };

        // Read the status register
        let status_reg_val: u32 = self.raw_device.read_reg(common_status_reg);

        if status_reg_val & CommonStatusRegister::Halted.bitmask() == 0 {
            // Device is not halted
            false
        } else {
            // Device is halted
            true
        }
    }

    // This function checks the status register to see if the device is idle
    fn idle(&self) -> bool {
        // Determine which status register needs to be read from depending on
        // the device capability.
        let common_status_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonStatus,
            TransferType::S2MM => Registers::S2mmCommonStatus,
        };

        // Read the status register
        let status_reg_val: u32 = self.raw_device.read_reg(common_status_reg);

        if status_reg_val & CommonStatusRegister::Idle.bitmask() == 0 {
            // Device is not idle
            false
        } else {
            // Device is idle
            true
        }
    }

    fn check_error(&self) -> Result<(), DmaCoreError> {
        // Determine which status register needs to be read from depending on
        // the device capability.
        let error_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sError,
            TransferType::S2MM => Registers::S2mmError,
        };

        // Read the status register
        let error_reg_val: u32 = self.raw_device.read_reg(error_reg);

        // Return the error value. We only return one error value. This is not
        // ideal as it may be the case that there are multiple error types
        // set.
        if error_reg_val & ErrorRegister::DmaIntErr.bitmask() != 0 {
            return Err(DmaCoreError::Internal);
        } else if error_reg_val & ErrorRegister::DmaSlvErr.bitmask() != 0 {
            return Err(DmaCoreError::Slave);
        } else if error_reg_val & ErrorRegister::DmaDecErr.bitmask() != 0 {
            return Err(DmaCoreError::Decode);
        } else if error_reg_val & ErrorRegister::SGIntErr.bitmask() != 0 {
            return Err(DmaCoreError::SGInternal);
        } else if error_reg_val & ErrorRegister::SGSlvErr.bitmask() != 0 {
            return Err(DmaCoreError::SGSlave);
        } else if error_reg_val & ErrorRegister::SGDecErr.bitmask() != 0 {
            return Err(DmaCoreError::SGDecode);
        } else {
            Ok(())
        }
    }

    fn get_and_consume_interrupts(
        &mut self,
        timeout: &time::Duration,
    ) -> Result<usize, DeviceError> {
        get_and_consume_interrupts(&mut self.file, timeout)
    }
}

impl DmaDevice for Device {
    fn borrow_dma_pool(&mut self) -> &mut MemPool {
        &mut self.dma_mem_pool
    }
}

impl ScatterGatherDevice for Device {
    fn borrow_descriptor_pool(&mut self) -> &mut DescriptorPool {
        &mut self.dma_descriptor_pool
    }
}

impl ScatterGatherConfig for Device {

    fn scatter_gather_config(&self) -> ScatterGatherDeviceConfig {
        let transfer_type = self.transfer_type;

        ScatterGatherDeviceConfig {
            dma_data_size: self.memory_pool_size(),
            dma_data_phys_addr: self.dma_data_phys_addr,
            max_bytes_transfer_per_descriptor: MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
            start_of_frame_bitmask: start_of_frame_bitmask(transfer_type),
            end_of_frame_bitmask: end_of_frame_bitmask(transfer_type),
            desc_field_nxt_desc_lsw_offset: desc_field_nxt_desc_lsw_offset(transfer_type),
            desc_field_nxt_desc_msw_offset: desc_field_nxt_desc_msw_offset(transfer_type),
            desc_field_buffer_addr_lsw_offset: desc_field_buffer_addr_lsw_offset(transfer_type),
            desc_field_buffer_addr_msw_offset: desc_field_buffer_addr_msw_offset(transfer_type),
            desc_field_control_offset: desc_field_control_offset(transfer_type),
        }
    }
}

impl Device {
    /// Represents a multi-channel scatter-gather device which it interacts
    /// with through the device file.
    ///
    /// Once created, DMA operations can be created and run on the device.
    pub fn new(device_path: &PathBuf) -> Result<Device, DeviceError> {
        // Initialise the device
        let mut device = Device::init(&device_path)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// Initialise the DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a DMA device.
    fn init(device_path: &PathBuf) -> Result<Device, DeviceError> {
        // Open the device file
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&device_path)?;

        let (cfg_mem, dma_data_mem, dma_data_phys_addr) =
            setup_cfg_and_data_mem::<Registers>(device_path, &file)?;

        // This needs to be done first, as it being a multichannel device
        // implies the registers are not set up properly.
        let (is_multi_channel, cfg_mem) = is_multi_channel_device(cfg_mem)?;

        let raw_device = RawDevice::<Registers>::new(cfg_mem)?;

        if !is_multi_channel {
            // Error if the device if a multi channel device
            return Err(DeviceError::UnsupportedUniChannel);
        }

        let transfer_type = get_transfer_type(&raw_device)?;

        let (dma_descriptors_mem, dma_descriptors_phys_addr) =
            setup_descriptors_mem(device_path, &file)?;

        let dma_descriptor_pool =
            unsafe { DescriptorPool::new(dma_descriptors_mem, dma_descriptors_phys_addr) };

        let dma_mem_pool = MemPool::new(dma_data_mem);

        #[cfg(target_arch = "arm")]
        {
            for n_ch in 0..MAX_CHANNELS {
                let channel = Channel::new(n_ch as u8).expect("This should not fail");

                // We never need the upper 32 bits of the configuration words on
                // 32-bit arm, so there is no point setting them at run time.
                // We zero them here though just to make sure nothing strange
                // happens.
                raw_device.write_reg(Registers::S2mmChCurrentDescriptorMsw(channel), 0u32);
                raw_device.write_reg(Registers::Mm2sChTailDescriptorMsw(channel), 0u32);
            }
        }

        Ok(Device {
            raw_device,
            dma_mem_pool,
            dma_data_phys_addr,
            dma_descriptor_pool,
            transfer_type,
            file,
            #[cfg(feature="async")]
            async_file: None,
        })
    }

    /// A method to create a new regular blocks operation on a single
    /// channel.
    ///
    /// * `block_size` - The size of each block in the memory.
    /// * `block_stride` - The offset difference between index 0 of block n and
    /// index 0 of block n+1 in the memory.
    /// * `n_blocks` - The number of blocks required.
    /// * `offset` - The offset in memory of index 0 of block 0.
    ///
    /// Memory claimed using this operation will be positioned at offset and
    /// span up to the end of the last block.
    ///
    /// Note: This method creates one descriptor per block so `n_blocks`
    /// should equal the number of AXIS packets you want to read from/write to
    /// memory.
    ///
    /// Note: For MM2S, `block_size` should equal the size of the AXIS packets
    /// you want to read from memory.
    ///
    /// Note: For S2MM, `block_size` should be greater than or equal to the size
    /// of the largest AXIS packet you want to write to memory.
    pub fn new_regular_blocks_operation(
        &mut self,
        block_size: usize,
        block_stride: usize,
        n_blocks: usize,
        offset: usize,
    ) -> Result<Operation, DeviceError> {
        let (descriptor_block, transfer_bytes, containing_memory) =
            write_regular_blocks_descriptor_chain(
                self,
                block_size,
                block_stride,
                n_blocks,
                offset,
            )?;

        let descriptor_functions = match self.transfer_type {
            TransferType::MM2S => DescriptorFunctions {
                descriptor_status: mm2s_descriptor_status,
                refresh_descriptor: refresh_mm2s_descriptor,
            },
            TransferType::S2MM => DescriptorFunctions {
                descriptor_status: s2mm_descriptor_status,
                refresh_descriptor: refresh_s2mm_descriptor,
            },
        };

        Ok(
            unsafe {
                Operation::new(
                    descriptor_block,
                    descriptor_functions,
                    transfer_bytes,
                    containing_memory)?
            }
        )
    }


    /// Implements the s2mm do_dma
    fn do_dma_s2mm(&mut self, operation: &MultiChannelOperation) -> Result<(), DeviceError> {
        //     Set Interrupt on Error ErrIrqEn
        //     Set Interrupt on Complete IocIrqEn
        //     Set Interrupt on error on other channel OtherChErrIrqEn
        //     Set Fetch
        //FIXME Should S2MM interrupt on packet drop?!
        let base_control_word: u32 = S2mmChannelControl::Fetch.bitmask()
            | S2mmChannelControl::OtherChErrIrqEn.bitmask()
            | S2mmChannelControl::IocIrqEn.bitmask()
            | S2mmChannelControl::ErrIrqEn.bitmask();

        enable_interrupts(&mut self.file)?;

        // Write the channels that are enabled
        self.raw_device.write_reg(
            Registers::S2mmChannelEnable,
            operation.channels_enabled_mask,
        );

        let irq_threshold_offset = S2mmChannelControl::IrqThreshold.offset();

        for (channel, ch_operation) in operation.iter() {
            // Set the current descriptor. This has to happen first.
            self.raw_device.write_reg(
                Registers::S2mmChCurrentDescriptorLsw(*channel),
                ch_operation.first_descriptor_lsw,
            );

            // Only need to write the upper 32 bits on 64-bit arch
            #[cfg(not(target_arch = "arm"))]
            {
                self.raw_device.write_reg(
                    Registers::S2mmChCurrentDescriptorMsw(*channel),
                    ch_operation.first_descriptor_msw,
                );
            }

            // Set the interrupt on complete threshold - we only interrupt when
            // we've handled all the descriptors.
            // FIXME this behaviour might be wrong for different descriptor
            // layouts.
            let channel_control_word =
                base_control_word | (ch_operation.n_descriptors << irq_threshold_offset) as u32;

            // Then turn on the fetch bit in the channel control register.
            // This MUST HAPPEN before the run/stop bit is set in the common
            // control.
            // This also sets the interrupts at the same time.
            self.raw_device
                .write_reg(Registers::S2mmChControl(*channel), channel_control_word);
        }

        // FIXME need barrier before run stop bit?

        // Set RunStop in common control reg. The current descriptors must
        // be already set up at this point.
        self.raw_device.write_reg(
            Registers::S2mmCommonControl,
            CommonControlRegister::RunStop.bitmask(),
        );

        // Finally write the tail descriptor addresses which begins the DMA.
        for (channel, ch_operation) in operation.iter() {
            // Set the current descriptor. This has to happen first.
            self.raw_device.write_reg(
                Registers::S2mmChTailDescriptorLsw(*channel),
                ch_operation.last_descriptor_lsw,
            );

            // Only need to write the upper 32 bits on 64-bit arch
            #[cfg(not(target_arch = "arm"))]
            {
                self.raw_device.write_reg(
                    Registers::S2mmChTailDescriptorMsw(*channel),
                    ch_operation.last_descriptor_msw,
                );
            }
        }

        Ok(())
    }

    /// Implements the mm2s do_dma
    fn do_dma_mm2s(&mut self, operation: &MultiChannelOperation) -> Result<(), DeviceError> {
        //     Set Interrupt on Error ErrIrqEn
        //     Set Interrupt on Complete IocIrqEn
        //     Set Interrupt on error on other channel OtherChErrIrqEn
        //     Set Fetch
        //FIXME Should S2MM interrupt on packet drop?!
        let base_control_word: u32 = Mm2sChannelControl::Fetch.bitmask()
            | Mm2sChannelControl::OtherChErrIrqEn.bitmask()
            | Mm2sChannelControl::IocIrqEn.bitmask()
            | Mm2sChannelControl::ErrIrqEn.bitmask();

        enable_interrupts(&mut self.file)?;

        // Write the channels that are enabled
        self.raw_device.write_reg(
            Registers::Mm2sChannelEnable,
            operation.channels_enabled_mask,
        );

        let irq_threshold_offset = Mm2sChannelControl::IrqThreshold.offset();

        for (channel, ch_operation) in operation.iter() {
            // Set the current descriptor. This has to happen first.
            self.raw_device.write_reg(
                Registers::Mm2sChCurrentDescriptorLsw(*channel),
                ch_operation.first_descriptor_lsw,
            );

            // Only need to write the upper 32 bits on 64-bit arch
            #[cfg(not(target_arch = "arm"))]
            {
                self.raw_device.write_reg(
                    Registers::Mm2sChCurrentDescriptorMsw(*channel),
                    ch_operation.first_descriptor_msw,
                );
            }

            // Set the interrupt on complete threshold - we only interrupt when
            // we've handled all the descriptors.
            // FIXME this behaviour might be wrong for different descriptor
            // layouts.
            let channel_control_word =
                base_control_word | (ch_operation.n_descriptors << irq_threshold_offset) as u32;

            // Then turn on the fetch bit in the channel control register.
            // This MUST HAPPEN before the run/stop bit is set in the common
            // control.
            // This also sets the interrupts at the same time.
            self.raw_device
                .write_reg(Registers::Mm2sChControl(*channel), channel_control_word);
        }

        // FIXME need barrier before run stop bit?

        // Set RunStop in common control reg. The current descriptors must
        // be already set up at this point.
        self.raw_device.write_reg(
            Registers::Mm2sCommonControl,
            CommonControlRegister::RunStop.bitmask(),
        );

        // Finally write the tail descriptor addresses which begins the DMA.
        for (channel, ch_operation) in operation.iter() {
            // Set the current descriptor. This has to happen first.
            self.raw_device.write_reg(
                Registers::Mm2sChTailDescriptorLsw(*channel),
                ch_operation.last_descriptor_lsw,
            );

            // Only need to write the upper 32 bits on 64-bit arch
            #[cfg(not(target_arch = "arm"))]
            {
                self.raw_device.write_reg(
                    Registers::Mm2sChTailDescriptorMsw(*channel),
                    ch_operation.first_descriptor_msw,
                );
            }
        }

        Ok(())
    }

    /// Perform a multi-channel scatter-gather DMA transfer.
    ///
    /// If successful this function returns a [RunningDevice], which can be
    /// waited on for completion of the DMA.
    pub fn do_dma(mut self, mut operation: MultiChannelOperation)
        -> Result<RunningDevice, MultiChannelTransferError>
    {
        let mut do_dma_inner = |operation: &mut MultiChannelOperation| {

            let mem_block = self.get_memory_from_operation(operation)?;

            match self.transfer_type {
                TransferType::MM2S => self.do_dma_mm2s(operation)?,
                TransferType::S2MM => self.do_dma_s2mm(operation)?,
            };

            Ok(mem_block)
        };

        match do_dma_inner(&mut operation) {
            Ok(mem_block) => Ok(RunningDevice {
                device: self,
                operation,
                mem_block,
            }),
            Err(e) => Err(MultiChannelTransferError {
                source: e,
                errored_device: (self, operation).into(),
            }),
        }
    }

    /// Performs a multi-channel scatter-gather DMA transfer in which the
    /// operation has pre-claimed the memory block. This is useful for 
    /// MM2S transfers in which you want an unbroken ownership chain of the
    /// memory.
    ///
    /// Beyond the signature, it behaves exactly like [do_dma].
    pub fn do_dma_with_mem(
        mut self, operation_with_mem: OperationWithMemory<MultiChannelOperation>)
        -> Result<RunningDevice, MultiChannelTransferError>
    {

        let (mut operation, mem_block):
            (MultiChannelOperation, MemBlock) = operation_with_mem.into();

        let mut do_dma_inner = |operation: &mut MultiChannelOperation| {

            if !operation.descriptors_from(&self.dma_descriptor_pool) {
                Err(DeviceError::WrongDevice)

            } else {

                match self.transfer_type {
                    TransferType::MM2S => self.do_dma_mm2s(operation)?,
                    TransferType::S2MM => self.do_dma_s2mm(operation)?,
                }
                Ok(())
            }
        };

        match do_dma_inner(&mut operation) {
            Ok(_) => Ok(RunningDevice {
                device: self,
                operation,
                mem_block,
            }),
            Err(e) => Err(MultiChannelTransferError {
                source: e,
                errored_device: (self, operation).into(),
            }),
        }


    }


    /// Return a memory block from the device memory pool.
    ///
    /// It is not possible to have live overlapping memory blocks. Until
    /// a memory block is freed, this method will error if an overlapping
    /// block is requested.
    ///
    /// Once the memory block is dropped, it relinquishes control of that
    /// region of memory.
    ///
    /// The returned memory block is both [Send] and [Sync] so can be passed
    /// around as needed.
    ///
    /// Note that when [do_dma] is called, it will first attempt to get hold
    /// of the necessary bit of memory, which will cause an error if the
    /// user owns an overlapping block.
    pub fn get_memory(&mut self, offset: usize, size: usize)
        -> Result<MemBlock, DeviceError>
    {
        let layout = MemBlockLayout { offset, size };
        Ok(self.dma_mem_pool.check_out_block(layout)?)
    }

    /// Returns the total size of the device memory pool.
    ///
    /// This number has nothing to do with the size of the blocks that can be
    /// checked out at this moment in time, which depends on what other blocks
    /// have been checked out. It is literally the total size of the memory
    /// pool.
    pub fn memory_pool_size(&self) -> usize {
        self.dma_mem_pool.size()
    }

    /// Returns the number of unused descriptors available on this device.
    pub fn n_available_descriptors(&self) -> Result<usize, DeviceError> {
        Ok(self.dma_descriptor_pool.descriptors_available())
    }

}

impl MemoryFromOperation for Device {

    fn get_memory_from_operation<T: ScatterGatherOperation>(&mut self, operation: &T)
        -> Result<MemBlock, DeviceError>
    {
        if !operation.descriptors_from(&self.dma_descriptor_pool) {
            Err(DeviceError::WrongDevice)
        } else {
            Ok(self.dma_mem_pool.check_out_block(operation.containing_memory())?)
        }
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod tests {
    use rand::{
        rngs::SmallRng,
        distributions::Standard,
        Rng,
        SeedableRng
    };
    use serial_test_derive::serial;
    use std::{
        cmp::min,
        collections::HashMap,
        path::PathBuf,
        ops::Range,
    };

    use super::{
        Channel, Device, MultiChannelTransferError,
        MAX_BYTES_TRANSFER_PER_DESCRIPTOR, MultiChannelOperation,
    };

    use crate::descriptors::DescriptorError;
    use crate::errors::DeviceError;
    use crate::scatter_gather::ScatterGatherError;
    use crate::operation::{
        MemoryFromOperation,
        ScatterGatherOperation,
    };

    /// The `dma::multi_channel::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {
        let device_path: PathBuf = ["/dev", "axi_dma_mc_combined"].iter().collect();

        let device = Device::new(&device_path);

        match device {
            Err(DeviceError::UnsupportedCapability) => (),
            _ => panic!("Did not get DeviceError::UnsupportedCapability error."),
        }
    }

    /// The `dma::multi_channel::device::new` function should return an
    /// UnsupportedUniChannel error if the underlying device is not a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_uni_channel() {
        let device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter().collect();

        let device = Device::new(&device_path);

        match device {
            Err(DeviceError::UnsupportedUniChannel) => (),
            _ => panic!("Did not get DeviceError::UnsupportedUniChannel error."),
        }
    }

    /// The `dma::multi_channel::device::new_single_channl_regular_blocks_operation`
    /// function should check that the `block_size` argument is in the range
    /// 8 -> 2**26 and that it is a multiple of 8. If either of these is not
    /// true then it should return a `InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();
        let mut rng = SmallRng::from_entropy();

        let block_size: usize = loop {
            // Generate a random block_size
            let block_size: usize = rng.gen();

            if block_size < 8 || block_size >= 1 << 26 || block_size % 8 != 0 {
                // Check that block_size is invalid, if it is then return it
                break block_size;
            }
        };

        let block_stride = block_size;
        let n_blocks = 1;
        let offset: usize = 0;

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidSize,
            }) => (),
            _ => panic!("Did not get InvalidSize error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `offset` argument is in the range
    /// 0 -> `memory_pool_size()` and that it is a multiple of 8. If either of
    /// these is not true then it should return a `InvalidOffset`
    /// error.
    #[test]
    #[serial]
    fn test_invalid_offset() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 16;
        let block_stride = block_size;
        let n_blocks = 1;

        let mut rng = SmallRng::from_entropy();
        let offset: usize = loop {
            // Generate a random offset
            let offset: usize = rng.gen();

            if offset >= device.memory_pool_size() || offset % 8 != 0 {
                // Check that offset is invalid, if it is then return it
                break offset;
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidOffset,
            }) => (),
            _ => panic!("Did not get InvalidOffset error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `block_stride` argument is in the range
    /// block_size -> `memory_pool_size()` and that it is a multiple of 8. If
    /// either of these is not true then it should return a
    /// `InvalidStride` error.
    #[test]
    #[serial]
    fn test_invalid_block_stride() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;
        let n_blocks = 1;

        let mut rng = SmallRng::from_entropy();
        let block_stride: usize = loop {
            // Generate a random block stride
            let block_stride: usize = rng.gen();

            if block_stride < block_size
                || block_stride >= device.memory_pool_size()
                || block_stride % 8 != 0
            {
                // Check that block_stride is invalid, if it is then
                // return it
                break block_stride;
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidStride,
            }) => (),
            _ => panic!("Did not get InvalidStride error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument does not exceed the
    /// maximum possible number of blocks given the block_stride. If it does
    /// then the function should return a `InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks_too_large() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = SmallRng::from_entropy();
        let block_stride: usize = 8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

        let min_n_blocks = device.memory_pool_size() / block_stride + 1;

        let n_blocks = rng.gen_range(min_n_blocks..min_n_blocks * 2);

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidNBlocks,
            }) => (),
            _ => panic!("Did not get InvalidNBlocks error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument is not zero.
    /// If it is, then the function should return a `InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks_zero() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = SmallRng::from_entropy();
        let block_stride: usize = 8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

        let n_blocks = 0;

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::InvalidNBlocks,
            }) => (),
            _ => panic!("Did not get InvalidNBlocks error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should return a DescriptorError() error if the
    /// device does not have sufficient descriptor chunks available to set up
    /// the requested operation.
    #[test]
    #[serial]
    fn test_insufficient_descriptors() {
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();

        let mut device = Device::new(&device_path).unwrap();
        let pool = &mut device.dma_descriptor_pool;

        // Set the block stride so that the blocks won't overlap
        let block_size = 1024;
        let block_stride = block_size;
        let offset = 0;

        let descriptors_available = pool.descriptors_available();

        // We check out every descriptor so we don't have enough.
        {
            // It's one descriptor per block, so this will use all the
            // descriptors and should not panic
            let _ops = device
                .new_regular_blocks_operation(
                    block_size,
                    block_stride,
                    descriptors_available,
                    offset,
                )
                .unwrap();

            let operation_res =
                device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

            match operation_res {
                Err(DeviceError::ScatterGather {
                    source:
                        ScatterGatherError::Descriptor {
                            source: DescriptorError::InsufficientDescriptors,
                        },
                }) => (),
                _ => panic!("Did not get InsufficientDescriptors error."),
            }
        }

        // Now we should be able to check some out again because the handle has
        // been dropped.
        let _ops = device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

        let _more_ops = device.new_regular_blocks_operation(
            block_size,
            block_stride,
            descriptors_available - 1,
            offset,
        );

        // And again, this should fail.
        let operation_res =
            device.new_regular_blocks_operation(block_size, block_stride, 1, offset);

        match operation_res {
            Err(DeviceError::ScatterGather {
                source:
                    ScatterGatherError::Descriptor {
                        source: DescriptorError::InsufficientDescriptors,
                    },
            }) => (),
            _ => panic!("Did not get InsufficientDescriptors error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `block_size`, `block_stride`,
    /// `n_blocks` and `offset` arguments will not cause a transfer to memory
    /// outside `dma_data`. If they do then it would cause a memory overflow.
    /// The function should prevent this by returning a
    /// `DeviceError::MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {
        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let max_block_size = min(device.memory_pool_size(), MAX_BYTES_TRANSFER_PER_DESCRIPTOR);

        let mut rng = SmallRng::from_entropy();
        let (block_size, block_stride, n_blocks, offset) = loop {
            // Generate a random nbytes

            // Randomly select arguments
            let block_size = 8 * rng.gen_range(1..max_block_size / 8 - 1);
            let block_stride: usize = 8 * rng.gen_range(block_size / 8..device.memory_pool_size() / 8);

            let max_n_blocks = device.memory_pool_size() / block_stride;
            let n_blocks = rng.gen_range(1..max_n_blocks + 1);

            let offset: usize = 8 * rng.gen_range(0..device.memory_pool_size() / 8);

            if (offset + block_stride * (n_blocks - 1) + block_size) > device.memory_pool_size() {
                // Check that nbytes and offset would result in a memory
                // overflow, if it would then return it
                break (block_size, block_stride, n_blocks, offset);
            }
        };

        let operation =
            device.new_regular_blocks_operation(block_size, block_stride, n_blocks, offset);

        match operation {
            Err(DeviceError::ScatterGather {
                source: ScatterGatherError::MemoryOverflow,
            }) => (),
            _ => panic!("Did not get MemoryOverflow error."),
        }
    }


    /// The `dma::multi_channel::device::do_dma` function should check that
    /// the `operation` argument was created by the device which is being
    /// called on to run the DMA. If it was not then it should return a
    /// `DeviceError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_do_dma_wrong_device() {
        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let device_0 = Device::new(&device_0_path).unwrap();

        // Create device 1
        let device_1_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path).unwrap();

        let operation_1 = device_1
            .new_regular_blocks_operation(block_size, block_stride, n_blocks, offset)
            .unwrap();

        // Create the operations hashmap
        let mut operations_on_1 = HashMap::new();
        operations_on_1.insert(Channel::new(0_u8).unwrap(), operation_1);

        let operations_on_1 = MultiChannelOperation::new(operations_on_1).unwrap();

        // Try to run do dma on an operation which was not created by the
        // device being called
        let MultiChannelTransferError {
            source: do_dma_err,
            errored_device: _,
        } = device_0.do_dma(operations_on_1).unwrap_err();

        match do_dma_err {
            DeviceError::WrongDevice => (),
            _ => panic!("Did not get DeviceError::WrongDevice error."),
        }
    }

    /// When an operation is combined with memory it should be accessible as
    /// though it was the separate.
    #[test]
    #[serial]
    fn test_operation_with_memory() {

        // Create device 0
        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let operation_1 = device
            .new_regular_blocks_operation(1024, 2048, 2, 0)
            .unwrap();

        let operation_12 = device
            .new_regular_blocks_operation(1024, 2048, 2, 3072)
            .unwrap();

        // Create the operations hashmap
        let mut operations = HashMap::new();
        operations.insert(Channel::new(1_u8).unwrap(), operation_1);
        operations.insert(Channel::new(12_u8).unwrap(), operation_12);

        let operations = MultiChannelOperation::new(operations).unwrap();

        let mut rng = SmallRng::from_entropy();
        let mut ref_data: Vec<u8> = (&mut rng)
            .sample_iter(&Standard)
            .take(device.memory_pool_size())
            .collect();

        let ref_slicer: Range<usize> = operations.containing_memory().into();
        {
            let mut mem = device.get_memory(0, device.memory_pool_size()).unwrap();
            mem.copy_from_slice(&ref_data);
        }

        {
            let mem = device.get_memory_from_operation(&operations).unwrap();
            assert_eq!(*mem, *ref_data.get(ref_slicer.clone()).unwrap());
        }

        let operations = {
            let op_mem = operations.claim_memory(&mut device).unwrap();
            assert_eq!(*op_mem, *ref_data.get(ref_slicer.clone()).unwrap());

            let (operations, memory) = op_mem.into();

            assert_eq!(*memory, *ref_data.get(ref_slicer.clone()).unwrap());
            operations
        };

        // Write through one way of getting the memory
        {
            let mut mem = device.get_memory_from_operation(&operations).unwrap();
            let mem_len = mem.len();
            mem.copy_from_slice(&vec![0u8; mem_len]);
        }

        // And then through another way.
        {
            let mut op_mem = operations.claim_memory(&mut device).unwrap();
            let mem_len = op_mem.len();
            op_mem.copy_from_slice(&vec![1u8; mem_len]);

            // Update the reference
            let sliced_ref_data = ref_data.get_mut(ref_slicer.clone()).unwrap();
            sliced_ref_data.copy_from_slice(&vec![1u8; mem_len]);
        }

        // Finally check the memory is still consistent
        let mem = device.get_memory(0, device.memory_pool_size()).unwrap();
        assert_eq!(*mem, *ref_data);
    }

}
