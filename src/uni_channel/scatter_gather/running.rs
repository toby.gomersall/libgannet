use crate::errors::DeviceError;
use crate::uni_channel::scatter_gather::{
    Device,
    ScatterGatherTransferError,
};

use crate::operation::{
    Operation,
    StaleOperation,
    RefreshStaleOperation,
    OperationError,
    OperationRefreshError,
};

use crate::{
    MemBlock,
    device_core::DeviceSpecifics,
    utils::get_and_consume_interrupts,
};

use std::time;

#[cfg(feature="async")]
use crate::utils::async_get_and_consume_interrupts;

#[cfg(feature="async")]
use tokio::{
    time::timeout,
    io::unix::AsyncFd,
};

#[derive(Debug)]
pub struct RunningDevice {
    pub(super) device: Device,
    pub(super) operation: Operation,
    pub(super) mem_block: MemBlock,
}

impl RunningDevice {
    fn inner_await_completed(&mut self, timeout_duration: time::Duration)
        -> Result<(), DeviceError>
    {
        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.
        if get_and_consume_interrupts(&mut self.device.file, &timeout_duration)? == 0 {
            // Poll timed out
            return Err(DeviceError::TimedOut);
        }

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > timeout_duration {time::Duration::from_secs(0)}
            else {timeout_duration - timeout_used};

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // If we are doing a transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine and the
        // descriptors have been updated properly.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the status register to clear the interrupt.
        self.device.unichannel_common.clear_all_interrupts();

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.unichannel_common.write_stop_bit();

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        self.device.wait_for_halted(&time::Duration::from_millis(10))?;

        Ok(())
    }

    #[cfg(feature="async")]
    async fn inner_completed(&mut self, timeout_duration: time::Duration)
        -> Result<(), DeviceError>
    {
        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.

        // We can't use `get_or_insert_with` here because the creation of
        // the AsyncFd object may error.
        if self.device.async_file.is_none() {
            self.device.async_file.replace(
                AsyncFd::new(self.device.file.try_clone()?)?);
        }

        let mut async_file = self.device.async_file.take().expect(
            "At this point, async_file should always contain something.");

        let _n_interrupts = match timeout(
            timeout_duration.clone(),
            async_get_and_consume_interrupts(&mut async_file))
            .await {
                Err(_) => return Err(DeviceError::TimedOut),
                Ok(read_result) => read_result?,
            };

        self.device.async_file.replace(async_file);

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > timeout_duration {time::Duration::from_secs(0)}
            else {timeout_duration - timeout_used};

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // If we are doing a transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine and the
        // descriptors have been updated properly.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the status register to clear the interrupt.
        self.device.unichannel_common.clear_all_interrupts();

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.unichannel_common.write_stop_bit();

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        self.device.wait_for_halted(&time::Duration::from_millis(10))?;

        Ok(())
    }

    /// Blocking wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// Once this method returns, the [Device] will be ready to use again.
    /// The associated operation is returned in the form of a
    /// [StaleOperation]. This must be refreshed to recover the
    /// useable [Operation].
    ///
    /// On error, a [ScatterGatherTransferError] will be returned which
    /// encapsulates the error information, as well as the [Device] and the
    /// associated [StaleOperation].
    pub fn await_completed(mut self, timeout: &time::Duration)
        -> Result<(Device, StaleOperation, MemBlock), ScatterGatherTransferError>
    {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_await_completed(timeout.clone());
        let Self { device, operation, mem_block } = self;

        match res {
            Err(err) => Err(ScatterGatherTransferError {
                source: err,
                errored_device: (device, operation.into()).into(),
            }),
            Ok(()) => Ok((device, operation.into(), mem_block)),
        }
    }

    /// Async wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// Once this method returns, the [Device] will be ready to use again.
    /// The associated operation is returned in the form of a
    /// [StaleOperation]. This must be refreshed to recover the
    /// useable [Operation].
    ///
    /// On error, a [ScatterGatherTransferError] will be returned which
    /// encapsulates the error information, as well as the [Device] and the
    /// associated [StaleOperation].
    pub async fn completed(mut self, timeout: &time::Duration)
        -> Result<(Device, StaleOperation, MemBlock), ScatterGatherTransferError>
    {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_completed(timeout.clone()).await;
        let Self { device, operation, mem_block } = self;

        match res {
            Err(err) => Err(ScatterGatherTransferError {
                source: err,
                errored_device: (device, operation.into()).into(),
            }),
            Ok(()) => Ok((device, operation.into(), mem_block)),
        }
    }
}

#[derive(Debug)]
pub struct ErroredRunningDevice {
    pub(super) device: Device,
    pub(super) stale_operation: StaleOperation,
}

impl From<RunningDevice> for ErroredRunningDevice {

    fn from(source: RunningDevice) -> Self {
        let RunningDevice { device, operation, mem_block:_ } = source;

        ErroredRunningDevice { device, stale_operation: operation.into() }
    }
}

impl From<(Device, Operation)> for ErroredRunningDevice {

    fn from((device, operation): (Device, Operation)) -> Self {

        ErroredRunningDevice { device, stale_operation: operation.into() }
    }
}

impl ErroredRunningDevice {

    /// Resets the errored device and refreshes the stale operation,
    /// returning the encapsulated device, the operation, and the first error
    /// reported through the descriptors.
    ///
    /// The first error reported through the descriptors is returned as the third
    /// element in the return tuple. This is useful for diagnosing _why_ the
    /// device errored. If no error was found then it will be `None`.
    pub fn reset(self) ->
        Result<(Device, Operation, Option<OperationError>), DeviceError>
    {
        let ErroredRunningDevice { mut device, stale_operation } = self;

        device.reset()?;

        match stale_operation.check_and_refresh() {
            Err(OperationRefreshError { source, refreshed_operation }) =>
                Ok((device, refreshed_operation, Some(source))),
            Ok(refreshed_operation) => Ok((device, refreshed_operation, None))
        }
    }

    /// Unwraps an ErroredRunningDevice into the [Device] and the
    /// [StaleOperation] from which the error occurred.
    ///
    /// Strictly speaking, the process of unwrapping is not unsafe, but the
    /// returned [Device] is in an undefined state. It is probably necessary
    /// to manually reset the device before it can be used again. Using it
    /// before it has been reset will result in undefined behaviour.
    pub unsafe fn unwrap(self) -> (Device, StaleOperation) {

        let ErroredRunningDevice { device, stale_operation } = self;

        (device, stale_operation)
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod device_tests {

    use crate::{
        uni_channel::scatter_gather::{
            Device,
            ScatterGatherTransferError,
        },
        errors::DeviceError,
    };
    use serial_test_derive::serial;
    use std::{
        path::PathBuf,
        time,
    };

    /// When a running device times out, it should return a
    /// ScatterGatherTransferError { DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[test]
    #[serial]
    fn test_scatter_gather_operation_timing_out() {
        // Create devices
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();

        // Create some trivial operations
        let s2mm_operation = s2mm_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        let mm2s_operation = mm2s_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.await_completed(&timeout) {
            Err(ScatterGatherTransferError {
                source: DeviceError::TimedOut, errored_device }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored.")
        };

        let (s2mm_device, s2mm_operation, errs) = errored_device.reset().unwrap();

        match errs {
            None => panic!("The checking of the descriptors should have errored."),
            Some(_) => ()
        }
        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(mm2s_operation).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation.await_completed(&timeout).unwrap();
        mm2s_running_operation.await_completed(&timeout).unwrap();
    }

}

#[cfg(all(test, target_arch = "arm", feature="async"))]
mod async_device_tests {

    use crate::{
        uni_channel::scatter_gather::{
            Device,
            ScatterGatherTransferError,
        },
        errors::DeviceError,
    };
    use serial_test_derive::serial;
    use std::{
        path::PathBuf,
        time,
    };

    use tokio;

    /// When a running device times out, it should return a
    /// ScatterGatherTransferError { DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[tokio::test]
    #[serial]
    async fn test_scatter_gather_operation_timing_out() {
        // Create devices
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_sg_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();

        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();

        // Create some trivial operations
        let s2mm_operation = s2mm_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        let mm2s_operation = mm2s_device
            .new_regular_blocks_operation(16, 16, 1, 0)
            .unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.completed(&timeout).await {
            Err(ScatterGatherTransferError {
                source: DeviceError::TimedOut, errored_device }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored.")
        };

        let (s2mm_device, s2mm_operation, errs) = errored_device.reset().unwrap();

        match errs {
            None => panic!("The checking of the descriptors should have errored."),
            Some(_) => ()
        }
        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(s2mm_operation).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(mm2s_operation).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation.completed(&timeout).await.unwrap();
        mm2s_running_operation.completed(&timeout).await.unwrap();
    }

}
