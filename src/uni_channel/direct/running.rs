use crate::errors::DeviceError;
use crate::uni_channel::{
    direct::{
        Device,
        DirectTransferError,
    },
    Registers,
};

use crate::{
    MemBlock,
    device_core::DeviceSpecifics,
    TransferType,
    utils::get_and_consume_interrupts,
};

use std::time;

#[cfg(feature="async")]
use crate::utils::async_get_and_consume_interrupts;

#[cfg(feature="async")]
use tokio::{
    time::timeout,
    io::unix::AsyncFd,
};

#[derive(Debug)]
pub struct RunningDevice {
    pub(super) device: Device,
    pub(super) mem_block: MemBlock,
    pub(super) expected_bytes: usize,
}

impl RunningDevice {
    fn inner_await_completed(&mut self, timeout_duration: time::Duration)
        -> Result<usize, DeviceError>
    {

        // Determine which length register needs to be used depending on the
        // transfer type.
        let length_reg = match self.device.unichannel_common.transfer_type {
            TransferType::MM2S => Registers::Mm2sLength,
            TransferType::S2MM => Registers::S2mmLength,
        };

        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.
        if get_and_consume_interrupts(&mut self.device.file, &timeout_duration)? == 0 {
            // Poll timed out
            return Err(DeviceError::TimedOut);
        }

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > timeout_duration {time::Duration::from_secs(0)}
            else {timeout_duration - timeout_used};

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // If we are doing an transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when all pending transactions have completed.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the status register to clear the interrupt.
        self.device.unichannel_common.clear_all_interrupts();

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.unichannel_common.write_stop_bit();

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        self.device.wait_for_halted(&time::Duration::from_millis(10))?;

        // Read the number of bytes transferred by the DMA device
        let bytes_transferred =
            self.device.unichannel_common.read_reg(length_reg) as usize;

        Ok(bytes_transferred)
    }

    #[cfg(feature="async")]
    async fn inner_completed(&mut self, timeout_duration: time::Duration)
        -> Result<usize, DeviceError>
    {

        // Determine which length register needs to be used depending on the
        // transfer type.
        let length_reg = match self.device.unichannel_common.transfer_type {
            TransferType::MM2S => Registers::Mm2sLength,
            TransferType::S2MM => Registers::S2mmLength,
        };

        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.

        // We can't use `get_or_insert_with` here because the creation of
        // the AsyncFd object may error.
        if self.device.async_file.is_none() {
            self.device.async_file.replace(
                AsyncFd::new(self.device.file.try_clone()?)?);
        }

        let mut async_file = self.device.async_file.take().expect(
            "At this point, async_file should always contain something.");

        let _n_interrupts = match timeout(
            timeout_duration.clone(),
            async_get_and_consume_interrupts(&mut async_file))
            .await {
                Err(_) => return Err(DeviceError::TimedOut),
                Ok(read_result) => read_result?,
            };

        self.device.async_file.replace(async_file);

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > timeout_duration {time::Duration::from_secs(0)}
            else {timeout_duration - timeout_used};

        // Check the error status of the DMA transfer.
        self.device.check_error()?;

        // If we are doing an transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when all pending transactions have completed.
        // When the device has returned to idle, it has definitely finished
        // all pending transactions.
        self.device.wait_for_idle(&timeout_remaining)?;

        // Write to the status register to clear the interrupt.
        self.device.unichannel_common.clear_all_interrupts();

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        self.device.unichannel_common.write_stop_bit();

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        self.device.wait_for_halted(&time::Duration::from_millis(10))?;

        // Read the number of bytes transferred by the DMA device
        let bytes_transferred =
            self.device.unichannel_common.read_reg(length_reg) as usize;

        Ok(bytes_transferred)
    }

    /// Blocking wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// The memory used by the transfer is returned as the second value in
    /// the return tuple. This memory block will be exactly the same size
    /// as the requested transfer.
    ///
    /// Once this method returns, the [Device] will be ready to use again.
    ///
    /// On error, a [DirectTransferError] will be returned which
    /// encapsulates the error information, as well as the [Device].
    pub fn await_completed(mut self, timeout_duration: &time::Duration)
        -> Result<(Device, MemBlock), DirectTransferError>
    {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_await_completed(timeout_duration.clone());
        let Self { device, mem_block, expected_bytes } = self;

        match res {
            Err(err) => Err(DirectTransferError::DeviceError {
                source: err,
                errored_device: device.into(),
            }),
            Ok(bytes_transferred) => {
                if bytes_transferred == expected_bytes {
                    Ok((device, mem_block))
                } else {
                    Err(DirectTransferError::TransferSize {
                        errored_device: device.into(),
                        transferred: bytes_transferred,
                        expected: expected_bytes,
                    })
                }
            },
        }
    }

    /// Async wait for completion of transfers on the running device, with
    /// a timeout.
    ///
    /// The memory used by the transfer is returned as the second value in
    /// the return tuple. This memory block will be exactly the same size
    /// as the requested transfer.
    ///
    /// Once this method returns, the [Device] will be ready to use again.
    ///
    /// On error, a [DirectTransferError] will be returned which
    /// encapsulates the error information, as well as the [Device].
    #[cfg(feature="async")]
    pub async fn completed(mut self, timeout_duration: &time::Duration)
        -> Result<(Device, MemBlock), DirectTransferError>
    {
        // Call inner_completed which borrows self mutably, so the usual ?
        // semantics work fine and we can still destructure self afterwards.
        let res = self.inner_completed(timeout_duration.clone()).await;
        let Self { device, mem_block, expected_bytes } = self;

        match res {
            Err(err) => Err(DirectTransferError::DeviceError {
                source: err,
                errored_device: device.into(),
            }),
            Ok(bytes_transferred) => {
                if bytes_transferred == expected_bytes {
                    Ok((device, mem_block))
                } else {
                    Err(DirectTransferError::TransferSize {
                        errored_device: device.into(),
                        transferred: bytes_transferred,
                        expected: expected_bytes,
                    })
                }
            },
        }
    }
}

#[derive(Debug)]
pub struct ErroredRunningDevice {
    pub(super) device: Device,
}

impl From<RunningDevice> for ErroredRunningDevice {

    fn from(source: RunningDevice) -> Self {
        let RunningDevice { device, ..} = source;

        ErroredRunningDevice { device }
    }
}

impl From<Device> for ErroredRunningDevice {

    fn from(device: Device) -> Self {

        ErroredRunningDevice { device }
    }
}

impl ErroredRunningDevice {

    /// Resets the errored device, returning the encapsulated device.
    pub fn reset(self) -> Result<Device, DeviceError>
    {
        let ErroredRunningDevice { mut device } = self;

        device.reset()?;
        Ok(device)
    }

    /// Unwraps an ErroredRunningDevice into the [Device].
    ///
    /// Strictly speaking, the process of unwrapping is not unsafe, but the
    /// returned [Device] is in an undefined state. It is probably necessary
    /// to manually reset the device before it can be used again. Using it
    /// before it has been reset will result in undefined behaviour.
    pub unsafe fn unwrap(self) -> Device {

        let ErroredRunningDevice { device } = self;
        device
    }
}

#[cfg(all(test, target_arch = "arm"))]
mod device_tests {

    use crate::{
        uni_channel::direct::{
            Device,
            DirectTransferError,
        },
        errors::DeviceError,
    };
    use serial_test_derive::serial;
    use std::{
        path::PathBuf,
        time,
    };

    /// When a running device times out, it should return a
    /// DirectTransferError { source: DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[test]
    #[serial]
    fn test_direct_dma_timing_out() {
        // Create devices
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_mm2s"].iter().collect();

        let s2mm_device = Device::new(&s2mm_device_path).unwrap();
        let mm2s_device = Device::new(&mm2s_device_path).unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(0, 16).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.await_completed(&timeout) {
            Err(DirectTransferError::DeviceError {
                source: DeviceError::TimedOut, errored_device }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored.")
        };

        let s2mm_device = errored_device.reset().unwrap();

        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(0, 16).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(0, 16).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation.await_completed(&timeout).unwrap();
        mm2s_running_operation.await_completed(&timeout).unwrap();
    }

}


#[cfg(all(test, target_arch = "arm", feature="async"))]
mod async_device_tests {

    use crate::{
        uni_channel::direct::{
            Device,
            DirectTransferError,
        },
        errors::DeviceError,
    };
    use serial_test_derive::serial;
    use std::{
        path::PathBuf,
        time,
    };

    use tokio;

    /// When a running device times out, it should return a
    /// DirectTransferError { source: DeviceError::TimedOut, ... }. The errored
    /// device (including the operation) once reset should be ready to use
    /// again.
    #[tokio::test]
    #[serial]
    async fn test_direct_dma_timing_out_async() {
        // Create devices
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_s2mm"].iter().collect();
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_mm2s"].iter().collect();

        let s2mm_device = Device::new(&s2mm_device_path).unwrap();
        let mm2s_device = Device::new(&mm2s_device_path).unwrap();

        // Firstly run the s2mm operation with no data, which should timeout
        let s2mm_running_operation = s2mm_device.do_dma(0, 16).unwrap();

        let timeout = time::Duration::from_millis(100);

        // Wait for DMA transfers to complete
        let errored_device = match s2mm_running_operation.completed(&timeout).await {
            Err(DirectTransferError::DeviceError {
                source: DeviceError::TimedOut, errored_device }) => errored_device,
            Err(e) => panic!("Received the wrong error (should be TimedOut): {:?}", e),
            Ok(_) => panic!("The operation should have errored.")
        };

        let s2mm_device = errored_device.reset().unwrap();

        // Now we should be able to try again...
        let s2mm_running_operation = s2mm_device.do_dma(0, 16).unwrap();
        let mm2s_running_operation = mm2s_device.do_dma(0, 16).unwrap();

        let timeout = time::Duration::from_millis(100);

        // This should work fine without error.
        s2mm_running_operation.completed(&timeout).await.unwrap();
        mm2s_running_operation.completed(&timeout).await.unwrap();
    }

}
