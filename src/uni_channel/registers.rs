use strum_macros::EnumIter;
use crate::{RegisterLayout, RegisterCheck};

// Register descriptions for the Xilinx AXI MCDMA LogiCORE IP.
// See Xilinx product guide pg021 for more information on the register space.
//
// In the LogiCORE IP AXI DMA block the configuration registers are 32 bits
// wide. Each 32 bit configuration register includes **four individually
// addressable** (8 bit wide) memory locations. In Libgannet, the
// configuration registers are treated as 32 bits wide therefore:
// Libgannet address | AXI DMA address      | Register
// ----------------- | -------------------- | ------------------------------
// 0                 | 0x00 0               | MM2S Control (MM2S_DMACR)
// 1                 | 0x04 4               | MM2S Status (MM2S_DMASR)
// 2                 | 0x08 8               | (SG) MM2S Current descriptor pointer LSW (MM2S_CURDESC)
// 3                 | 0x0C 12              | (SG) MM2S Current descriptor pointer MSW (MM2S_CURDESC_MSB)
// 4                 | 0x10 16              | (SG) MM2S Tail descriptor pointer LSW (MM2S_TAILDESC)
// 5                 | 0x14 20              | (SG) MM2S Tail descriptor pointer MSW (MM2S_TAILDESC_MSB)
// 6                 | 0x18 24              | (Direct) MM2S Start address LSW (MM2S_SA)
// 7                 | 0x1C 28              | (Direct) MM2S Start address MSW (MM2S_SA_MSB)
// 8                 | 0x20 32              | -
// 9                 | 0x24 36              | -
// 10                | 0x28 40              | (Direct) MM2S Transfer Length (MM2S_LENGTH)
// 11                | 0x2C 44              | -
// 12                | 0x30 48              | S2MM Control (S2MM_DMACR)
// 13                | 0x34 52              | S2MM Status (S2MM_DMASR)
// 14                | 0x38 56              | (SG) S2MM Current descriptor pointer LSW (S2MM_CURDESC)
// 15                | 0x3C 60              | (SG) S2MM Current descriptor pointer MSW (S2MM_CURDESC_MSB)
// 16                | 0x40 64              | (SG) S2MM Tail descriptor pointer LSW (S2MM_TAILDESC)
// 17                | 0x44 68              | (SG) S2MM Tail descriptor pointer MSW (S2MM_TAILDESC_MSB)
// 18                | 0x48 72              | (Direct) S2MM Destination address LSW (S2MM_DA)
// 19                | 0x4C 76              | (Direct) S2MM Destination address MSW (S2MM_DA_MSB)
// 20                | 0x50 80              | -
// 21                | 0x54 84              | -
// 22                | 0x58 88              | (Direct) S2MM Transfer Length (S2MM_LENGTH)
#[derive(Debug, EnumIter)]
pub enum Registers {
    Mm2sControlRegister,            // MM2S_DMACR
    Mm2sStatusRegister,             // MM2S_DMASR
    Mm2sSGCurrentDescriptorLsw,     // MM2S_CURDESC
    Mm2sSGCurrentDescriptorMsw,     // MM2S_CURDESC_MSB
    Mm2sSGTailDescriptorLsw,        // MM2S_TAILDESC
    Mm2sSGTailDescriptorMsw,        // MM2S_TAILDESC_MSB
    Mm2sStartAddressLsw,            // MM2S_SA
    Mm2sStartAddressMsw,            // MM2S_SA_MSB
    Mm2sLength,                     // MM2S_LENGTH
    S2mmControlRegister,            // S2MM_DMACR
    S2mmStatusRegister,             // S2MM_DMASR
    S2mmSGCurrentDescriptorLsw,     // S2MM_CURDESC
    S2mmSGCurrentDescriptorMsw,     // S2MM_CURDESC_MSB
    S2mmSGTailDescriptorLsw,        // S2MM_TAILDESC
    S2mmSGTailDescriptorMsw,        // S2MM_TAILDESC_MSB
    S2mmDestAddressLsw,             // S2MM_SA
    S2mmDestAddressMsw,             // S2MM_SA_MSB
    S2mmLength,                     // S2MM_LENGTH
}

impl RegisterLayout for Registers {

    type RegisterType = u32;

    fn offset(&self) -> usize {
        match *self {
            Registers::Mm2sControlRegister => 0,
            Registers::Mm2sStatusRegister => 1,
            Registers::Mm2sSGCurrentDescriptorLsw => 2,
            Registers::Mm2sSGCurrentDescriptorMsw => 3,
            Registers::Mm2sSGTailDescriptorLsw => 4,
            Registers::Mm2sSGTailDescriptorMsw => 5,
            Registers::Mm2sStartAddressLsw => 6,
            Registers::Mm2sStartAddressMsw => 7,
            Registers::Mm2sLength => 10,
            Registers::S2mmControlRegister => 12,
            Registers::S2mmStatusRegister => 13,
            Registers::S2mmSGCurrentDescriptorLsw => 14,
            Registers::S2mmSGCurrentDescriptorMsw => 15,
            Registers::S2mmSGTailDescriptorLsw => 16,
            Registers::S2mmSGTailDescriptorMsw => 17,
            Registers::S2mmDestAddressLsw => 18,
            Registers::S2mmDestAddressMsw => 19,
            Registers::S2mmLength => 22,
        }
    }
}

impl RegisterCheck for Registers {}

#[derive(EnumIter)]
pub enum ControlRegister {
    RunStop,          // RW Write 1 to trigger a run, 0 to stop.
    RegisterExists,   // RO Always reads 1 if the register exists
    Reset,            // RW 1 to trigger a soft reset.
    _Keyhole,         // RW 1 enables keyhole read
    _CyclicBD,        // RW 1 enables cyclic BD
    IocIrqEn,         // RW 1 enables interrupt on complete.
    _DlyIrqEn,        // RW 1 enables interrupt on delay timer (SG only)
    ErrIrqEn,         // RW 1 enables interrupt on error
    IrqThreshold,     // Interrupt threshold
    _IrqDelay,        // Interrupt delay timeout
}
impl ControlRegister {
    pub fn offset(&self) -> u32 {
        match *self {
            ControlRegister::RunStop => 0,
            ControlRegister::RegisterExists => 1,
            ControlRegister::Reset => 2,
            ControlRegister::_Keyhole => 3,
            ControlRegister::_CyclicBD => 4,
            ControlRegister::IocIrqEn => 12,
            ControlRegister::_DlyIrqEn => 13,
            ControlRegister::ErrIrqEn => 14,
            ControlRegister::IrqThreshold => 16,
            ControlRegister::_IrqDelay => 24,
        }
    }
    pub fn bitwidth(&self) -> u32 {
        match *self {
            ControlRegister::RunStop => 1,
            ControlRegister::RegisterExists => 1,
            ControlRegister::Reset => 1,
            ControlRegister::_Keyhole => 1,
            ControlRegister::_CyclicBD => 1,
            ControlRegister::IocIrqEn => 1,
            ControlRegister::_DlyIrqEn => 1,
            ControlRegister::ErrIrqEn => 1,
            ControlRegister::IrqThreshold => 8,
            ControlRegister::_IrqDelay => 8,
        }
    }
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}

pub enum StatusRegister {
    Halted,           // RO 1 if DMACR_RUN_STOP and operations halted.
    Idle,             // RO 1 if DMA controller is paused
    SGIncId,          // RO 1 if scatter gather enabled
    DmaIntErr,        // RO 1 if DMA internal error
    DmaSlvErr,        // RO 1 if DMA slave error
    DmaDecErr,        // RO 1 if DMA decode error
    SGIntErr,         // RO 1 if scatter gather internal error
    SGSlvErr,         // RO 1 if scatter gather slave error
    SGDecErr,         // RO 1 if scatter gather decode error
    IocIrq,           // R/WC 1 if IOC interrupt event happened. Write 1 to clear
    DlyIrq,           // R/WC 1 if delay interrupt event happened. Write 1 to clear
    ErrIrq,           // R/WC 1 if error interrupt event happened. Write 1 to clear
    _IrqThreshold,     // Current interrupt threshold
    _IrqDelay,        // Current interrupt delay timeout
}
impl StatusRegister {
    pub fn offset(&self) -> u32 {
        match *self {
            StatusRegister::Halted => 0,
            StatusRegister::Idle => 1,
            StatusRegister::SGIncId => 3,
            StatusRegister::DmaIntErr => 4,
            StatusRegister::DmaSlvErr => 5,
            StatusRegister::DmaDecErr => 6,
            StatusRegister::SGIntErr => 8,
            StatusRegister::SGSlvErr => 9,
            StatusRegister::SGDecErr => 10,
            StatusRegister::IocIrq => 12,
            StatusRegister::DlyIrq => 13,
            StatusRegister::ErrIrq => 14,
            StatusRegister::_IrqThreshold => 16,
            StatusRegister::_IrqDelay => 24,
        }
    }
    pub fn bitwidth(&self) -> u32 {
        match *self {
            StatusRegister::Halted => 1,
            StatusRegister::Idle => 1,
            StatusRegister::SGIncId => 1,
            StatusRegister::DmaIntErr => 1,
            StatusRegister::DmaSlvErr => 1,
            StatusRegister::DmaDecErr => 1,
            StatusRegister::SGIntErr => 1,
            StatusRegister::SGSlvErr => 1,
            StatusRegister::SGDecErr => 1,
            StatusRegister::IocIrq => 1,
            StatusRegister::DlyIrq => 1,
            StatusRegister::ErrIrq => 1,
            StatusRegister::_IrqThreshold => 8,
            StatusRegister::_IrqDelay => 8,
        }
    }
    pub fn bitmask(&self) -> u32 {
        (2_u32.pow(self.bitwidth()) - 1) << self.offset()
    }
}
