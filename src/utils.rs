use std::{
    fs::File,
    io::Read,
    io::Write,
    os::unix::io::AsRawFd,
    time,
};

use nix::poll::{
    poll,
    PollFd,
    PollFlags,
};
use memmap::MmapMut;
use strum_macros::EnumIter;

use crate::RegisterLayout;
use crate::raw_device::RawDevice;
use crate::errors::DeviceError;

#[cfg(feature="async")]
use tokio::{
    io::unix::AsyncFd,
};

#[derive(Debug, EnumIter)]
enum MultiChannelEnableRegisters {
    Mm2sChannelEnable,
    S2mmChannelEnable
}

impl RegisterLayout for MultiChannelEnableRegisters {

    type RegisterType = u32;

    fn offset(&self) -> usize {
        match *self {
            MultiChannelEnableRegisters::Mm2sChannelEnable => 2,   // (2 * 4 = 0x008)
            MultiChannelEnableRegisters::S2mmChannelEnable => 322, // (322 * 4 = 0x508)
        }
    }
}

/// This function tests the device file to see if an interrupt has been
/// received. It will wait up to `timeout_ms` milliseconds for an interrupt.
/// If an error is not encountered this function will always return
/// `Ok(n_files_interrupted)` where n_files_interrupted represents the number
/// of files that have interrupted since the file was last `read`. This
/// function can only receive a single file so n_files_interrupted can be a
/// maximum of 1. If n_files_interrupted is not zero, the file has interrupted.
///
/// This function timed out if n_files_interrupted = 0.
///
/// If this function detects any interrupts on the file it will consume them.
pub(crate) fn get_and_consume_interrupts(
    file: &mut File, timeout_duration: &time::Duration) -> Result<usize, DeviceError> {

    // Convert timeout from duration to integer
    let timeout_ms = timeout_duration.as_millis() as i32;

    let poll_fd = PollFd::new(file.as_raw_fd(), PollFlags::POLLIN);

    // Check to see if the file is ready for reading.
    let n_files_interrupted = poll(&mut [poll_fd], timeout_ms)?;

    if n_files_interrupted > 0 {
        // Means an interrupt was received and the file is ready for reading
        // so consume the interrupt and return.

        // Read the file to consume any previously received interrupts.
        // Interrupt buffer needs to be big enough to take 4 bytes.
        let mut interrupt_buffer = [0u8; 4];
        file.read(&mut interrupt_buffer)?;
    }

    Ok(n_files_interrupted as usize)
}

/// Async equivalent of get_and_consume_interrupts without a timeout.
#[cfg(feature="async")]
pub(crate) async fn async_get_and_consume_interrupts(file: &mut AsyncFd<File>)
    -> Result<usize, DeviceError>
{
    let mut interrupt_buffer = [0u8; 4];
    loop {
        let mut guard = file.readable_mut().await?;

        // Once we poll to read, we runner a closure on the inner file.
        //
        match guard.try_io(|inner| inner.get_mut().read(&mut interrupt_buffer)) {
            Ok(result) => return Ok(result?),
            Err(_would_block) => continue,
        }
    }
}
//#[cfg(feature="async")]
//pub(crate) async fn async_get_and_consume_interrupts_with_timeout(
//    file: &mut AsyncFd<File>,
//    timeout_duration: &time::Duration) -> Result<usize, DeviceError> {
//
//    match timeout(timeout_duration, async_get_and_consume_interrupts).await {
//        Err(_) => DeviceError::TimedOut,
//        Ok(read_result) => read_result?,
//    }
//}

/// This function enables interrupts in the UIO driver for the device
/// represented by file.
pub(crate) fn enable_interrupts(file: &mut File) -> Result<(), DeviceError> {
    // Enable interrupts in the uio driver by writing a 1 to the uio
    // file. Note: write_all takes a vector of u8 values but we need
    // to write 32 bits to the file, hence why we have four u8 values
    // in the vector.
    let irq_enable = [1u8, 0u8, 0u8, 0u8];
    file.write_all(&irq_enable)?;

    // flush is required to make sure the data is written through the
    // cache to device. Without flush it is possible to get race
    // conditions whereby the interrupt returns before the device file
    // interrupts are enabled.
    file.flush()?;

    Ok(())
}

/// This function checks if the device defined by `register_mem` is a multi
/// channel device.
///
/// It temporarily takes ownership of `register_mem`, returning it at
/// completion.
pub(crate) fn is_multi_channel_device(registers_mem: MmapMut) 
    -> Result<(bool, MmapMut), DeviceError>
{

    // if the device is multi channel then either multi channel MM2S or multi
    // channel S2MM must be enabled. If either of these direction checks
    // returns true then it must be a multi channel device.
    // If both of them return false then it must not be a multi channel
    // device.
    //
    // In the multi-channel device:
    //     Offset 0x008 = MM2S ChannelEnable register
    //     bit 0 is Read/Write
    //     Offset 0x508 = S2MM ChannelEnable register
    //         bit 0 is Read/Write
    //
    // In the non multi-channel device:
    //     Offset 0x008 = Mm2sSGCurrentDescriptorLsw register
    //         bit 0 is Read only and always returns 0
    //     Offset 0x508 = Outside device register space but less than one page
    //                    size (4096). Therefore it is safe to write to this
    //                    address as we assign a full page to each device.
    //
    // Based on the above if we can set bit 0 in the register at offsets 0x008
    // or 0x508 then the device must be multi channel and transfer_type must
    // be enabled. If we cannot then the device is either not multi channel or
    // transfer_type is not enabled on the device.
    //
    let mc_check_device = RawDevice::<MultiChannelEnableRegisters>::new(registers_mem)?;

    mc_check_device.write_reg(MultiChannelEnableRegisters::Mm2sChannelEnable, 1);
    mc_check_device.write_reg(MultiChannelEnableRegisters::S2mmChannelEnable, 1);

    let read_mm2s = mc_check_device.read_reg(MultiChannelEnableRegisters::Mm2sChannelEnable);
    let read_s2mm = mc_check_device.read_reg(MultiChannelEnableRegisters::S2mmChannelEnable);

    // mask out the bottom 6 bits in the MM2s register.
    // These are the RO bits on the single-channel device for address 0x8.
    // Technically, it's possible for the other bits to be non-zero, so we 
    // mitigate that risk with this mask.
    // We don't need to do this for the s2mm device because the check is 
    // outside the address space of the single-channel device.
    let read_mm2s = read_mm2s & 0b111111;

    Ok(((read_mm2s == 1) || (read_s2mm == 1), mc_check_device.unwrap()))
}


